import streamlit as st

from langchain.callbacks import StreamlitCallbackHandler
from langchain.llms import LlamaCpp

#####################################################
#
# Static Variables
#
#####################################################

if not hasattr(st, 'llm'):
    st.llm = LlamaCpp(
        model_path="../models/mixtral-8x7b-v0.1.Q4_K_M.gguf",
        temperature=0.75,
        max_tokens=2000,
        n_ctx=2048,
        top_p=1,
        verbose=True,  # Verbose is required to pass to the callback manager
    )

#####################################################
#
# Session Variables
#
#####################################################

if "messages" not in st.session_state:
    st.session_state.messages = []
    print('here')

#####################################################
#
# Streamlit APP
#
#####################################################

title = "LLM chat"
st.set_page_config(page_title=title, page_icon=":robot:")
st.header(title)


for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("Ask to mistral"):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

    with st.chat_message("assistant"):
        message_placeholder = st.empty()
        st_callback = StreamlitCallbackHandler(message_placeholder)
        full_response = st.llm(prompt, callbacks=[st_callback])
        message_placeholder.markdown(full_response)
    # Add assistant response to chat history
    st.session_state.messages.append(
        {"role": "assistant", "content": full_response})
