from llama_cpp import Llama

# Set gpu_layers to the number of layers to offload to GPU. Set to 0 if no GPU acceleration is available on your system.
llm = Llama(
    # Download the model file first
    model_path="../models/mixtral-8x7b-v0.1.Q4_K_M.gguf",
    n_ctx=2048,  # The max sequence length to use - note that longer sequence lengths require much more resources
    # The number of CPU threads to use, tailor to your system and the resulting performance
    n_threads=8,
    # The number of layers to offload to GPU, if you have GPU acceleration available
    n_gpu_layers=0,
    # chat_format="llama-2"
)

# Simple inference example
# output = llm(
#   "Ciao come stai?", # Prompt
#   max_tokens=512,  # Generate up to 512 tokens
#   stop=["</s>"],   # Example stop token - not necessarily correct for this specific model! Please check before using.
#   echo=True        # Whether to echo the prompt
# )

# Chat Completion API
# llm = Llama(model_path="./mixtral-8x7b-v0.1.Q4_K_M.gguf", chat_format="llama-2")  # Set chat_format according to the model you are using
result = llm.create_chat_completion(
    messages=[
        {"role": "system", "content": "You are a story writing assistant."},
        {
            "role": "user",
            "content": "Write a story about llamas."
        }
    ],
    stream=True,
)
for mex in result:
    if 'role' in mex['choices'][0]['delta'].keys():
        print(mex['choices'][0]['delta']['role'], ':')
    else:
        print('\t', mex['choices'][0]['delta']['content'])
