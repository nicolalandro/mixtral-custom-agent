from langchain.chains import ConversationChain
from langchain.llms import CTransformers

config = {'max_new_tokens': 2048, 'repetition_penalty': 1.1}

llm = CTransformers(
    model='../models/mistral-7b-instruct-v0.1.Q4_K_M.gguf',
    config=config,
    model_type='mistral',
    gpu_layers=0,  # CPU
)

chain = ConversationChain(llm=llm)

print(chain.run(input='Mi saluti in italiano?'))
