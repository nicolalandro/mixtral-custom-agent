import os
from transformers import AutoModelForCausalLM, AutoTokenizer

cache_dir = '../models'

device = "cuda"  # the device to load the model onto
model = AutoModelForCausalLM.from_pretrained(
    "mistralai/Mixtral-8x7B-v0.1",
    cache_dir=cache_dir,
    device_map="cuda"
)
tokenizer = AutoTokenizer.from_pretrained(
    "mistralai/Mistral-8x7B", cache_dir=cache_dir)

prompt = "My favourite condiment is"
model_inputs = tokenizer([prompt], return_tensors="pt").to(device)
model.to(device)
generated_ids = model.generate(
    **model_inputs, max_new_tokens=100, do_sample=True)
tokenizer.batch_decode(generated_ids)[0]
