from langchain.llms import OpenAI
from langchain.agents import AgentType, initialize_agent, load_tools
from langchain.callbacks import StreamlitCallbackHandler
from langchain.llms import LlamaCpp

import streamlit as st



llm = LlamaCpp(
    model_path="../models/mixtral-8x7b-v0.1.Q4_K_M.gguf",
    temperature=0.75,
    max_tokens=2000,
    n_ctx=2048,
    top_p=1,
    verbose=True,  # Verbose is required to pass to the callback manager
)

tools = load_tools(["ddg-search"])
agent = initialize_agent(
    tools, llm, agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION, verbose=True
)

if prompt := st.chat_input():
    st.chat_message("user").write(prompt)
    with st.chat_message("assistant"):
        st_callback = StreamlitCallbackHandler(st.container())
        response = agent.run(prompt, callbacks=[st_callback])
        st.write(response)