import streamlit as st

from ctransformers import AutoModelForCausalLM

#####################################################
#
# Static Variables
#
#####################################################

if not hasattr(st, 'llm'):

    st.llm = AutoModelForCausalLM.from_pretrained(
        "../models/mistral-7b-instruct-v0.1.Q4_K_M.gguf",
        model_file="mistral-7b-instruct-v0.1.Q4_K_M.gguf",
        model_type="mistral",
        gpu_layers=0,
    )

#####################################################
#
# Session Variables
#
#####################################################

if "messages" not in st.session_state:
    st.session_state.messages = []

#####################################################
#
# Streamlit APP
#
#####################################################

title = "LLM chat"
st.set_page_config(page_title=title, page_icon=":robot:")
st.header(title)


for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("Ask to mistral"):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

    # response = st.chain.run(input=prompt)
    # TODO https://python.langchain.com/docs/integrations/llms/bedrock#conversation-chain-with-streaming
    full_response = ""
    with st.chat_message("assistant"):
        message_placeholder = st.empty()
        for chunk in st.llm(prompt, stream=True):
            # print(chunk)
            full_response += chunk
            message_placeholder.markdown(full_response + "▌")
        message_placeholder.markdown(full_response)
    # Add assistant response to chat history
    st.session_state.messages.append(
        {"role": "assistant", "content": full_response})
