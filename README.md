# Mixtral agent
In this repo I will try to create an agent with mixtral model.

* mistral guuf with langchain
```
cd src
huggingface-cli download TheBloke/Mistral-7B-Instruct-v0.1-GGUF mistral-7b-instruct-v0.1.Q4_K_M.gguf --local-dir ./models --local-dir-use-symlinks False

python langchain_ctransformers.py
```
* streamlit mistral
```
cd src
streamlit run streamlit_mistral.py
```
* mixtral
```
huggingface-cli download TheBloke/Mixtral-8x7B-v0.1-GGUF mixtral-8x7b-v0.1.Q4_K_M.gguf --local-dir ./models --local-dir-use-symlinks False

cd src
python mixtral_transformers.py
python mixtral_gguf.py
```

## References
* [Mixtral](https://huggingface.co/docs/transformers/model_doc/mixtral): get started
* Agents
  * [Transformers](https://huggingface.co/docs/transformers/transformers_agents): huggingface implementations
  * [Langchain agent](https://python.langchain.com/docs/modules/agents/)
  * [Langchain agent with local LLM](https://github.com/langchain-ai/langchain/discussions/11429)
  * [Custom agent with easyllm](https://philschmid.github.io/easyllm/examples/llama2-agent-example/#basic-example-of-using-a-tool-with-llama-2-70b)
  * [Langchain and Ctransformers](https://python.langchain.com/docs/integrations/providers/ctransformers)
  * [llama-cpp-python](https://llama-cpp-python.readthedocs.io/en/latest/api-reference/#llama_cpp.llama_types.ChatCompletionStreamResponseDeltaEmpty)
  * [langchain + llamacpp](https://python.langchain.com/docs/integrations/llms/llamacpp)
  + [langchain duck duck go search](https://python.langchain.com/docs/integrations/tools/ddg)
* Streamlit
  * [LLM chat](https://docs.streamlit.io/knowledge-base/tutorials/build-conversational-apps)
  * [Langchain chat](https://github.com/langchain-ai/streamlit-agent/blob/main/streamlit_agent/minimal_agent.py)
